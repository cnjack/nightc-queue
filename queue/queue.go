package queue

type Queue struct {
	queue chan string
}

func NewQueue(size int) *Queue {
	ch := make(chan string, size)
	return &Queue{ch}
}

func (this *Queue) Push(q string) {
	this.queue <- q
}

func (this *Queue) Pull() string {
	if len(this.queue) == 0 {
		return ""
	} else {
		return <-this.queue
	}
}

func (this *Queue) Count() int {
	return len(this.queue)
}
