#nightc-queue
A FIFO queue writed by golang
##description 
A FIFO queue writed by golang
##auth
nightc
##email
admin#nightc.com(replace # by @)
##Install
go get -u git.oschina.net/cnjack/nightc-queue  
go install git.oschina.net/cnjack/nightc-queue  
win  
copy the config.ini to your bin  
%GOPATH%\bin\nightc-queue  
##API
default post is 8080  
http://{yourserver}/create?d={yourdatabase}&s={databasesize}  
http://{yourserver}/push?d={yourdatabase}&q={query}  
http://{yourserver}/pull?d={yourdatabase}  
http://{yourserver}/count?d={yourdatabase}  
##list of the spider
+home-[http://git.oschina.net/cnjack/nightc-spider](http://git.oschina.net/cnjack/nightc-spider)  
+downloader-[http://git.oschina.net/cnjack/nightc-download](http://git.oschina.net/cnjack/nightc-download)  
+queue-[http://git.oschina.net/cnjack/nightc-queue](http://git.oschina.net/cnjack/nightc-queue)   